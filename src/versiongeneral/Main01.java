package versiongeneral;

import versiongeneral.business.Child;
import versiongeneral.business.Parent;
import versiongeneral.business.ParentGrand;
import versiongeneral.interfaze.IGetNodeAdvance;
import versiongeneral.interfaze.IGetNodeBase;

public class Main01 {

    public static void main(String[] args) {

        IGetNodeBase nodeVersionParentGrant = new ParentGrand();

        IGetNodeAdvance nodeVersionParent = new Parent();

        IGetNodeAdvance nodeVersionChild = new Child();


        run(nodeVersionParentGrant);

        run(nodeVersionParent);

        run(nodeVersionChild);


        System.out.println("--------------");


        runBoth(nodeVersionChild);

        runBoth(nodeVersionParent);
    }

    /*
            Base düzeyde Default-Version arayüzüne sahip tüm client'lar bu işlevi başarılı bir şekilde çağırabilirler.
     */
    static void run(IGetNodeBase mNodeVersion) {

        mNodeVersion.onNodeDefaultVersion();
    }

    // Advance-Node-Version'a sahip tüm client'lar bu işlevi başarılı bir şekilde çağırabilirler.
    static void runBoth(IGetNodeAdvance mNodeVersion) {

        mNodeVersion.onNodeDefaultVersion();

        mNodeVersion.onNodeAdvanceVersion();
    }

    /*
                * Interface Segregation client'ı, kullanmadığı işlevleri implement etmek zorunda kalmaktan kurtarır.
                  Bu prensibin gereği, client;
                        1- İşlevsiz kod bloklarına sahip olmamış olur
                        2- Gerekli olmayan metod imzalarını uygulamak zorunda olmamış olur.

                  Böylece, gerekli değişikliklerin yan etkilerini ve sıklığını azalmış olur.

                * Bu teknik uygulanırken, en temel seviyede minimum miktarda işlevler eklenen BaseInterface sağlanır.
                  İlgili client'lar da bu interface'i uygular.
                  Daha sonra her eklenecek yeni işlevler için BaseInterface'den türeyen yeni ChildInterface'ler üretilerek,
                  ilgili client'lar da bu yeni ChildInterface'leri uygular.

                  --- Bu şekilde ilerlemede, yazılan interfaceler, client'lar için, Role Interface görevi görür.
                      Bu şekilde çok tanımlanmış olan interface'ler, daha kolay bir şekilde, yeniden yapılandırılabilir,
                      değiştirilebilir, ve yeniden dağıtılabilir özelliğe sahiptir.
                  --- Bir başka yararı da, ince taneli yazılan base interface'ler, büyüdüklerinde daha anlamlı isimlerle
                      tasarlanabilirler. Bu da bize isimlendirme konularında daha noktasal çalışma olanağı sağlar.

                  Temelde Child'lardan Parent'a doğru ilerledikçe yazılan her interface, Odaklanmış birimsel hale gelir.
                  Bu da 'fat' veya 'polluted' diye anılan şişirilmiş interface'lerden kaçınmamızı sağlamış olur.

    */

}

package versiongeneral.business;

import versiongeneral.interfaze.IGetNodeBase;

public class ParentGrand implements IGetNodeBase {

    private final String iAm = getClass().getSimpleName();

    @Override
    public void onNodeDefaultVersion() {

        System.out.println(iAm + ": Node Default Version: 1.0");
    }
}

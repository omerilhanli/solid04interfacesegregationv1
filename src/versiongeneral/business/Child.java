package versiongeneral.business;

import versiongeneral.interfaze.IGetNodeAdvance;

public class Child implements IGetNodeAdvance {

    private final String iAm = getClass().getSimpleName();

    @Override
    public void onNodeDefaultVersion() {

        System.out.println(iAm + ": Node Default Version 2.0");
    }

    @Override
    public void onNodeAdvanceVersion() {

        System.out.println(iAm + ": Node Advance Version 2.8");
    }
}

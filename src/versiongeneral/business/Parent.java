package versiongeneral.business;

import versiongeneral.interfaze.IGetNodeAdvance;

public class Parent implements IGetNodeAdvance {

    private final String iAm = getClass().getSimpleName();

    @Override
    public void onNodeDefaultVersion() {

        System.out.println(iAm + ": Node Default Version: 1.0");
    }

    @Override
    public void onNodeAdvanceVersion() {

        System.out.println(iAm + ": Node Advance Version: 1.6");
    }
}

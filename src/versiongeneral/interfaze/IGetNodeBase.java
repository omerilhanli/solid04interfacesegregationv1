package versiongeneral.interfaze;

/*

        - Default olarak hem ParentGrand hem Parent hem de Child için geçerli bir Default-Node-Version mevcut.
          Bu yüzden bu arayüz her üç somut class tarafından uygulanmak zorundadır.
 */
public interface IGetNodeBase {

    void onNodeDefaultVersion();
}

package versiongeneral.interfaze;

/*

        - Tüm class'lar için Default-Node-Version yanında bir de Advance-Node-Version için bu arayüz implement edilir.
 */
public interface IGetNodeAdvance extends IGetNodeBase {

    void onNodeAdvanceVersion();
}

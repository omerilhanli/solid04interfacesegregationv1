package versionspesifical.business;

import versionspesifical.interfaze.IDeveloperIOS;

public class IosDeveloper extends DeveloperBase implements IDeveloperIOS {

    @Override
    public void publishAsIos() {

        System.out.println(iAm + ":: I can write IOS applications with ObjectiveC/Swift Language.");
    }
}

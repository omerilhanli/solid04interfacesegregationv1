package versionspesifical.business;

import versionspesifical.interfaze.IDeveloperAndroid;

public class AndroidDeveloper extends DeveloperBase implements IDeveloperAndroid {

    @Override
    public void publishAsAndroid() {

        System.out.println(iAm + "I can write ANDROID applications with Java/Kotlin Language.");
    }
}

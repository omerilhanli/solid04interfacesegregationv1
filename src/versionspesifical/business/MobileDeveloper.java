package versionspesifical.business;

import versionspesifical.interfaze.IDeveloperMobile;

public class MobileDeveloper extends DeveloperBase implements IDeveloperMobile {

    @Override
    public void publishAsAndroid() {

        System.out.println(iAm + "I can write ANDROID applications with Java/Kotlin Language.");
    }

    @Override
    public void publishAsIos() {

        System.out.println(iAm + "I can write IOS applications with ObjectiveC/Swift Language.");
    }
}

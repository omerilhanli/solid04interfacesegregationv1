package versionspesifical.business;

import versionspesifical.interfaze.IDeveloperFullstack;

public class FullstackDeveloper extends DeveloperBase implements IDeveloperFullstack {

    @Override
    public void publishAsBackend() {

        System.out.println(iAm + "I can write BACKEND applications with Any OOP Language.");
    }

    @Override
    public void publishAsAndroid() {

        System.out.println(iAm + "I can write ANDROID applications with Java/Kotlin Language.");
    }

    @Override
    public void publishAsIos() {

        System.out.println(iAm + "I can write IOS applications with ObjectiveC/Swift Language.");
    }

    @Override
    public void publishAsWeb() {

        System.out.println(iAm + "I can write WEB applications with Javascript/Php Language.");
    }
}

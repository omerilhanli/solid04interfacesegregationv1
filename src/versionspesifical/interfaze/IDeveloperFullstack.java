package versionspesifical.interfaze;

public interface IDeveloperFullstack extends IDeveloperWeb, IDeveloperMobile {

    @Override
    default void MyJobTitle(){

        System.out.println("I am Full Stack Developer");
    }

    void publishAsBackend();
}

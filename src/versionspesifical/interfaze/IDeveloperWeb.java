package versionspesifical.interfaze;

public interface IDeveloperWeb extends IDeveloperBase {

    @Override
    default void MyJobTitle(){

        System.out.println("I am WEB Developer");
    }

    void publishAsWeb();
}

package versionspesifical.interfaze;

public interface IDeveloperAndroid extends IDeveloperBase {

    @Override
    default void MyJobTitle(){

        System.out.println("I am Android Developer");
    }

    void publishAsAndroid();
}

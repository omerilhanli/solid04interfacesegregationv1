package versionspesifical.interfaze;

public interface IDeveloperMobile extends IDeveloperIOS, IDeveloperAndroid {

    @Override
    default void MyJobTitle(){

        System.out.println("I am Mobile Developer");
    }

}

package versionspesifical.interfaze;

public interface IDeveloperIOS extends IDeveloperBase {

    @Override
    default void MyJobTitle(){

        System.out.println("I am IOS Developer");
    }

    void publishAsIos();
}

package versionspesifical;

import versionspesifical.business.*;
import versionspesifical.interfaze.*;

public class Main02 {

    static void lineBreak() {
        System.out.println("--------------------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {

        //----------------------------------------------------------------
        IDeveloperAndroid devAndroid = new AndroidDeveloper();

        IDeveloperIOS devIos = new IosDeveloper();

        IDeveloperMobile devMobile = new MobileDeveloper();

        IDeveloperWeb devWeb = new WebDeveloper();

        IDeveloperFullstack devFullstack = new FullstackDeveloper();
        //----------------------------------------------------------------


        // Android
        runAndroid(devAndroid);
        runAndroid(devMobile);
        runAndroid(devFullstack);
        //.. Android, Mobile ve Full Stack developer'lar ANDROID application yazabilirler.


        lineBreak();


        // IOS
        runIos(devIos);
        runIos(devMobile);
        runIos(devFullstack);
        //.. IOS, Mobile ve Full Stack developer'lar IOS application yazabilirler.


        lineBreak();


        // Mobile
        runMobile(devMobile);
        runMobile(devFullstack);
        //.. Hem Mobile hem Full Stack developer'lar MOBILE application yazabilirler.


        lineBreak();


        // Web
        runWeb(devWeb);
        runWeb(devFullstack);
        //.. Hem Web hem Full Stack developer'lar WEB application yazabilirler.


        lineBreak();


        // Full Stack
        runFullstack(devFullstack);
        //.. Yalnızca Full Stack Developer'lar; hem android, hem ios, hem web hem de backend application yazabilirler.
    }

    static void runAndroid(IDeveloperAndroid developer) {

        developer.publishAsAndroid();
    }

    static void runIos(IDeveloperIOS developer) {

        developer.MyJobTitle();

        developer.publishAsIos();
    }

    static void runWeb(IDeveloperWeb developer) {

        developer.MyJobTitle();

        developer.publishAsWeb();
    }

    static void runMobile(IDeveloperMobile developer) {

        developer.MyJobTitle();

        developer.publishAsAndroid();

        developer.publishAsIos();
    }

    static void runFullstack(IDeveloperFullstack developer) {

        developer.MyJobTitle();

        developer.publishAsAndroid();

        developer.publishAsIos();

        developer.publishAsWeb();

        developer.publishAsBackend();
    }
}

/*
                * Burada tüm IDeveloperBase temelli Interface'ler ince taneli olarak ayrılmış olarak yazılır.
                  Bu sayede istenen işlevler için; daha bir isteğe yönelik kısıtlanmış olurlar. Gelen yeni geliştirmeler,
                  daha yapılandırılabilir, değiştirilebilir, kolayca dağıtılarak uygulanabilir ve shotgun effect'i azaltılmış olur.
                  Bu sayede geliştirmelerde performans artışı sağlanır.
 */
